-- ---------------
-- assignment4.sql
-- ---------------

\c;

set client_min_messages=warning;
drop table if exists BOOK;
drop table if exists PHYSICALCOPY;
drop table if exists READER;
drop table if exists LOAN;

\echo #1
create table BOOK (
    ISBN      int,
    title     text,
    authors   text,
    publisher text, 
	year      int,
	price     float);
	
create table PHYSICALCOPY (
    catalogNo           int,
    ISBN                int,
    location            text,
    maxDaysLoan         int,
	overdueChargePerDay float);
	
create table READER (
    userName          text,
    name              text,
    address           text,
    maxNoBooksForLoan int);
	
create table LOAN (
    userName  text,
    catalogNo int,
    dateOut   date,
    dateIn    date);

\echo #2
insert into BOOK values (111,'Fundamentals of Database','Fares', 'Addison-Wesley',2000,10.00);
insert into BOOK values (222,'Cat in the Hat','Dr.Seuss', 'Addison-Wesley',2001,5.00);
insert into BOOK values (333,'The Giving Tree','Shel Silverstein', 'Addison-Wesley',2000,7.50);
insert into BOOK values (444,'Hungry Catepillar','Eric Carle', 'Addison-Wesley',2003,12.00);
insert into BOOK values (555,'One Piece','Eiichiro Oda', 'Shonen Jump',1977,7.99);
insert into BOOK values (666,'Game of Thrones','George R.R. Martin', 'HBO Max',2000,30.00);

insert into PHYSICALCOPY values (123,111,'PCL',5,1.99);
insert into PHYSICALCOPY values (345,222,'Classics Library',4,3.99);
insert into PHYSICALCOPY values (546,333,'Classics Library',2,6.99);
insert into PHYSICALCOPY values (456,444,'Classics Library',1,4.99);
insert into PHYSICALCOPY values (678,555,'Life Sciences Library',3,2.99);
insert into PHYSICALCOPY values (573,666,'PCL',7,4.99);

insert into READER values ('tvn397','Nam Nguyen','123 Fake Street',4);
insert into READER values ('abb128','Alec Biggerstaff','234 Real Street',1);
insert into READER values ('fff546','Fares Fraij','189 CS Road',2);
insert into READER values ('rck789','Rocket Stonks','156 Moon Drive',3);
insert into READER values ('gs324','Goldy Smith','234 Pearl Street',1);

insert into LOAN values ('tvn397',123,'2021-04-15','2021-04-16');
insert into LOAN values ('tvn397',345,'2021-04-15',null);
insert into LOAN values ('tvn397',456,'2021-04-15','2021-04-25');
insert into LOAN values ('abb128',123,'2021-04-17','2021-04-20');
insert into LOAN values ('abb128',573,'2021-04-15','2021-04-16');
insert into LOAN values ('abb128',678,'2021-04-15','2021-04-19');
insert into LOAN values ('gs324',123,'2021-04-21','2021-04-21');
insert into LOAN values ('gs324',678,'2021-04-20',null);
insert into LOAN values ('gs324',456,'2021-04-26','2021-04-28');
insert into LOAN values ('gs324',546,'2021-04-15',null);
insert into LOAN values ('rck789',573,'2021-04-17',null);
insert into LOAN values ('fff546',546,'2021-04-28',null);

\echo #3
select distinct title, authors, price
	from BOOK
	where 
		publisher = 'Addison-Wesley'
		and
		year = 2000
	order by title;
	
\echo #4
select distinct title
	from BOOK
	where ISBN in
		(select ISBN
			from PHYSICALCOPY
			where maxDaysLoan > 3);

\echo #5
select count(catalogNo)
	from PHYSICALCOPY
	where catalogNo in
		(select catalogNo
			from LOAN
			where
				userName = 'Goldy Smith'
				and
				dateIn is null);

\echo #6
select distinct name, count(userName)
	from READER
	inner join LOAN on READER.userName = LOAN.userName
	where maxNoBooksForLoan < count
			(select userName
			from LOAN
			where dateIn is null)

\echo #7
select distinct name
	from READER
	where userName in
		(select userName
			from LOAN
			where catalogNo in
				(select catalogNo
					from PHYSICALCOPY
					where ISBN in
						(select ISBN
							from BOOK
							where title = 'Fundamentals of Database')));
	
\echo #8
select name, address, title
	from READER 
	inner join LOAN on READER.userName = LOAN.userName
	inner join PHYSICALCOPY on LOAN.catalogNo = PHYSICALCOPY.catalogNo
	inner join BOOK on PHYSICALCOPY.ISBN = BOOK.ISBN
	where userName in
		(select userName
			from LOAN
			inner join PHYSICALCOPY using(catalogNo)
			where 
				dateIn is null
				and
				datediff(current_date - dateOut) > maxDaysLoan)

\echo #9
drop table if exists LOAN;
\quit